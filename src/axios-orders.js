import Axios from 'axios';

const incAxios = Axios.create({
   baseURL: 'https://fiil-burger.firebaseio.com/',
});

export default incAxios;