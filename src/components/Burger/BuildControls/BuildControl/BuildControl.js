import React from 'react';

import classes from './BuildControl.module.css';


const BuildControl = (props) => (
   <div className={classes.BuildControl}>
      <div className={classes.Lebel}>{props.label}</div>
      <div className={classes.ButtonControl}>
         <button 
            className={classes.Less} 
            onClick={props.removeIngredient} 
            disabled={props.disabled}
         >Less</button>
         <button 
            className={classes.More} 
            onClick={props.addIngredient}
         >More</button>
      </div>
   </div>
);

export default BuildControl;