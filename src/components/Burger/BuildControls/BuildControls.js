import React from 'react';

import CurrencyFormat from 'react-currency-format';

import BuildControl from './BuildControl/BuildControl';
import classes from './BuildControls.module.css';

const controls = [
   {label: 'Salad', type: 'salad'},
   {label: 'Bacon', type: 'bacon'},
   {label: 'Cheese', type: 'cheese'},
   {label: 'Meat', type: 'meat'}
];

const BuildControls = (props) => (
   <div className={classes.BuildControls}>
      <p>Current Price: <strong><CurrencyFormat value={props.totalPrice} displayType={'text'} thousandSeparator={true} prefix={'Rp '} /></strong></p>
      {controls.map(ctrl => (
         <BuildControl 
            key={ctrl.label} 
            label={ctrl.label}
            addIngredient={()=>props.addIngredient(ctrl.type)}
            removeIngredient={()=>props.removeIngredient(ctrl.type)}
            disabled={props.disabled[ctrl.type]}
         />
      ))}
      <button 
         className={classes.OrderButton} 
         disabled={props.purchasable}
         onClick={props.ordered}
      >ORDER NOW</button>
   </div>
);

export default BuildControls;