import React from 'react';
import Button from '../../UI/Button/Button';

import CurrencyFormat from 'react-currency-format';
import classes from './OrderSummary.module.css';


const OrderSummary = (props) => {

   const ingredientSummary = Object.keys(props.ingredients)
      .map(igKey => {
         return (
            <li key={igKey}>
               <span style={{textTransform: 'capitalize'}}>{igKey}</span>: {props.ingredients[igKey]}
            </li>
            );
      });
   return (
      <>
         <h3>Your Order</h3>
         <p>A delicious burger with the following ingredients:</p>
         <ul>
            {ingredientSummary}
         </ul>
         <p><strong>Total price: <CurrencyFormat value={props.totalPrice} displayType={'text'} thousandSeparator={true} prefix={'Rp '} /></strong></p>
         <p>Continue to Checkout??</p>
         <div className={classes.Button}>
            <Button btnType="Danger" onClick={props.purchaseCancel}>CANCEL</Button>
            <Button btnType="Success" onClick={props.purchaseContinue}>CONTINUE</Button>
         </div>

      </>
   )
};

export default OrderSummary;