import React from 'react';
import { NavLink } from 'react-router-dom';
import classes from './NavigationItem.module.css';

const NavigationItem = (props) => (
   <li className={classes.NavigationItem}>
      <NavLink to={props.link} exact onClick={props.onClick} activeClassName={classes.active}><strong>{props.children}</strong></NavLink>
   </li>
);

export default NavigationItem;