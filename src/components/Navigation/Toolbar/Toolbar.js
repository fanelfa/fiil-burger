import React from 'react';
import classes from './Toolbar.module.css';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';

const Toolbar = (props) => (
   <header className={classes.Toolbar}>
      <DrawerToggle className={classes.DrawerToggle} onClick={props.toggleSideDrawer}/> 
      <Logo height="80%"/>
      <nav>
         <NavigationItems />
      </nav>
   </header>
);

export default Toolbar;