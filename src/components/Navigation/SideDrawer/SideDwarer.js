import React from 'react';
import Logo from '../../Logo/Logo';
import Backdrop from '../../UI/Backdrop/Backdrop';
import NavigationItems from '../NavigationItems/NavigationItems';
import classes from './SideDrawer.module.css';

const SideDrawer = (props) => {

   let attachedClasses = [classes.SideDrawer, classes.Close];
   if(props.isOpen){
      attachedClasses = [classes.SideDrawer, classes.Open];
   }
   return (
      <>
         <div className={attachedClasses.join(' ')}>
            <Logo height="9%"/>
            <nav>
               <NavigationItems/>
            </nav>
         </div>
         <Backdrop show={props.isOpen} onClick={props.onClick}/>
      </>
   );
};

export default SideDrawer;