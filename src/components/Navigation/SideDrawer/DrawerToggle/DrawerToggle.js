import classes from './DrawerToggle.module.css';
import React from 'react';

const DrawerToggle = (props) => (
   <div 
   onClick={props.onClick}
   className={classes.DrawerToggle}>
      <span></span>
      <span></span>
      <span></span>
   </div>
);

export default DrawerToggle;