import classes from './Input.module.css';
import React from 'react'

const Input = (props) => {
   let classValid = '';
   let errorMessage = null;
   if(!props.valid && props.touched){
      classValid = classes.Invalid;
      errorMessage = <p className={classes.ErrorMessage}>Please input valid data</p>
   }

   const inputClasses = [classes.Input, classValid].join(' ');
   let inputElement;
   switch(props.element){
      case ('input'):
         inputElement = <input className={inputClasses} {...props.elementConfig} onChange={props.onChange} value={props.value}/>;
         break;
      case ('textarea'):
         inputElement = <textarea className={inputClasses} {...props.elementConfig} onChange={props.onChange} value={props.value}/>;
         break;
      case ('select'):
         inputElement = <select className={inputClasses} {...props.elementConfig} onChange={props.onChange} value={props.value}>
            {
               props.elementConfig.options.map((option)=>(
                  <option value={option.value} key={option.value} className={classes.Option}>{option.displayValue}</option>
               ))
            }
         </select>;
         break;
      default:
         inputElement = <input className={inputClasses} {...props.elementConfig} onChange={props.onChange} value={props.value} />;
   }
   return (
      <div className={classes.Item}>
         <label className={classes.Label}>{props.label}</label>
         {inputElement}
         {errorMessage}
      </div>
   );
};


export default Input;