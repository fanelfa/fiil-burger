import React, { memo } from 'react';
import Backdrop from '../Backdrop/Backdrop';
import classes from './Modal.module.css';

const Modal = (props) => {

   return (
   <>
      <div 
         className={classes.Modal}
         style={{
            transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
            opacity: props.show ? '1' : '0'
         }}>
         {props.children}
      </div>
      <Backdrop show={props.show} onClick={props.modalClose}/>
   </>
)};

// export default Modal;

function areEqual(prevProps, nextProps) {
   return prevProps.show === nextProps.show && prevProps.children === nextProps.children;
}

export default memo(Modal, areEqual);