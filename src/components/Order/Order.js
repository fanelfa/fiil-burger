import classes from './Order.module.css';
import React from 'react'
import CurrencyFormat from 'react-currency-format';

const Order = (props) => {
   const ingredients = Object.keys(props.ingredients).map((igName)=>{
      if(props.ingredients[igName]!==0){
         return <span key={igName} 
         style={{
            textTransform:'capitalize',
            display:'inline-block',
            margin:'0 8px',
            padding:'5px 7px',
            borderRadius:'5px',
            backgroundColor:'#fff',
            boxShadow:'inset 0 1px 5px rgba(109, 159, 139, 0.356)'
            }}>{igName + ' ('} <strong>{props.ingredients[igName]}</strong> {')'}</span>
      }
      return null;
   });
   // const ingredients = [];
   // for (let ingredientName in props.ingredients){
   //    ingredients.push({
   //       name: ingredientName,
   //       amount: props.ingredients[ingredientName]
   //    });
   // }
   // const ingredientOutput = ingredients.map(ig=>{
   //    return <span key={ig.name}>{ig.name} ({ig.amount})</span>
   // });
   return (
      <div className={classes.Order}>
         <p>Ingredients: {ingredients}</p>
         <p>Price: <strong><CurrencyFormat value={props.price} displayType={'text'} thousandSeparator={true} prefix={'Rp '} /></strong></p>
      </div>
   );
};

export default Order;