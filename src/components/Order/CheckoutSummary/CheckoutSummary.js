import classes from './CheckoutSummary.module.css';
import React from 'react';
import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';

const CheckoutSummary = (props) => {
   return (
      <div className={classes.CheckoutSummary}>
         <h3>We hope it tastes well!</h3>
         <div style={{width:'100%', margin:'auto'}}>
            <Burger ingredients={props.ingredients} />
         </div>
         <div className={classes.Button}>
            <Button btnType="Danger" onClick={props.onCheckoutCanceled}>CANCEL</Button>
            <Button btnType="Success" onClick={props.onCheckoutContinued}>CONTINUE</Button>
         </div>
      </div>
   );
};

export default CheckoutSummary;