import React, { Component } from 'react';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDwarer';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';

import classes from './Layout.module.css';

class Layout extends Component{
   state = {
      showSideDrawer: false
   }
   sideDrawerCloseHandler = () => {
      this.setState({showSideDrawer: false});
   }

   sideDrawerToggleHandler = () => {
      this.setState((prevState)=>{
         return { showSideDrawer: !prevState.showSideDrawer };
      });
   }
   render(){
      return(
         <>
            <Toolbar toggleSideDrawer={this.sideDrawerToggleHandler}/>
            <SideDrawer 
               isOpen={this.state.showSideDrawer}
               onClick={this.sideDrawerCloseHandler}
            />
            <main className={classes.Content}>
               {this.props.children}
            </main>
         </>
      );
   }
}

export default Layout;