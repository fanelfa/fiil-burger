import React, { Component } from 'react';
import incAxios from '../../axios-orders';
import Order from '../../components/Order/Order';
import Spinner from '../../components/UI/Spinner/Spinner';
import classes from './Orders.module.css';

class Orders extends Component{
   state={
      orders: null,
      loading: true
   }
   componentDidMount() {
      incAxios.get('/orders.json')
         .then(res => {
            // const fetchedOrders = [];
            // for (let key in res.data){
            //    fetchedOrders.push({...res.data[key], id: key});
            // }
            this.setState({ orders: res.data, loading:false });
         }).catch(err => {
            this.setState({ error: true, loading: false  });
         });
   }

   objectToComponent=(obj)=>{
      return Object.keys(obj).map((key)=>{
         let order = obj[key];
         return <Order key={key} ingredients={order.ingredients} price={order.totalPrice}/>
      });
   }

   render() {
      return this.state.loading?
      <Spinner/>
      :(
         <div className={classes.Orders}>
            {this.state.orders ? this.objectToComponent(this.state.orders):null}
         </div>
      )
   }
}

export default Orders;
