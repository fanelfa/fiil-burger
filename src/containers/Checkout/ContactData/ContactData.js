import classes from './ContactData.module.css';
import React, { Component } from 'react';
import incAxios from '../../../axios-orders';
import Spinner from '../../../components/UI/Spinner/Spinner';
import { withRouter } from 'react-router';
import Input from '../../../components/UI/Input/Input';

class ContactData extends Component {
   state = {
      loading: false,
      ingredients: null,
      formData: {
         nama: {
            elementType: 'input',
            elementConfig: {
               name: 'nama', type: 'text', placeholder: 'Your name'
            },
            label: 'Nama',
            value: '',
            validation: {
               required: true
            },
            valid: false,
            touched: false
         },
         email: {
            elementType: 'input',
            elementConfig: {
               name: 'email', type: 'email', placeholder: 'Your email'
            },
            label: 'Email',
            value: '',
            validation: {
               required: true
            },
            valid: false,
            touched: false
         },
         street: {
            elementType: 'textarea',
            elementConfig: {
               name: 'street', type: 'text', placeholder: 'Alamat lengkap'
            },
            label: 'Alamat',
            value: '',
            validation: {
               required: true,
               minLength: 5
            },
            valid: false,
            touched: false
         },
         city: {
            elementType: 'input',
            elementConfig: {
               name: 'city', type: 'text', placeholder: 'Kota anda'
            },
            label: 'Kota',
            value: '',
            validation: {
               required: true,
               minLength: 5
            },
            valid: false,
            touched: false
         },
         deliverMethod: {
            elementType: 'select',
            elementConfig: {
               name: 'deliverMethod',
               options: [
                  { value: '', displayValue: 'Pilih Metode Pengiriman' },
                  { value: 'fastest', displayValue: 'Fastest' },
                  { value: 'standard', displayValue: 'Standard' },
               ]
            },
            label: 'Kota',
            value: '',
            validation: {
               required: true
            },
            valid: false,
            touched: false
         }
      },
      allInputValid: false
   }

   checkValidity(value, rules) {
      let isValid = true;
      if (!rules) {
         return true;
      }

      if (rules.required) {
         isValid = value.trim() !== '' && isValid;
      }

      if (rules.minLength) {
         isValid = value.trim().length >= rules.minLength && isValid;
      }

      return isValid;
   }

   onChangeHandler = (e) => {
      const name = e.target.name;
      const value = e.target.value;
      this.setState((prevState) => {
         return {
            ...prevState,
            formData: {
               ...prevState.formData,
               [name]: {
                  ...prevState.formData[name],
                  value: value,
                  valid: this.checkValidity(value, prevState.formData[name].validation),
                  touched: true
               }
            }
         }
      });
      this.setState((prevState) => {
         let formValid = true;
         Object.keys(prevState.formData).map(key => {
            formValid = prevState.formData[key].valid && formValid;
            return true;
         });
         return {
            allInputValid: formValid
         }
      })
   }

   onSubmit = (e) => {
      e.preventDefault();
      // console.log(this.props);
      const data = this.state.formData;
      if (this.state.allInputValid) {
         const contactData = {
            nama: data['nama'].value,
            email: data['email'].value,
            address: {
               street: data['street'].value,
               city: data['city'].value
            }
         };
         // console.log(contactData);
         this.setState({ loading: true });
         const order = {
            ingredients: this.props.ingredients,
            totalPrice: this.props.price,
            customer: contactData,
            deliverMethod: data.deliverMethod.value === '' ? 'standard' : data.deliverMethod.value
         }
         incAxios.post('/orders.json', order).then(res => {
            // console.log(res);
            this.setState({
               loading: false,
               ingredients: null,
            });
            this.props.history.push('/');
         }).catch(err => {
            // console.log(err);
            this.setState({ loading: false, purchasing: false });
         });
      } else {
         alert('input tidak valid');
      }
   }

   render() {
      let component = (
         <div className={classes.ContactData} >
            <h4>Please Enter Your Contact Data</h4>
            <form onSubmit={this.onSubmit}>
               {
                  Object.keys(this.state.formData).map(key => {
                     const data = this.state.formData[key];
                     return <Input
                        element={data.elementType}
                        onChange={this.onChangeHandler}
                        key={data.elementConfig.name}
                        elementConfig={data.elementConfig}
                        label={data.label}
                        value={data.value}
                        valid={data.valid}
                        touched={data.touched}
                     />
                  })
               }
               <br />
               <button className={classes.Button} type="submit" disabled={this.state.allInputValid ? false : true}>Submit</button>
            </form>
         </div>
      );
      if (this.state.loading) {
         component = <Spinner />
      }

      return component;
   }
}
export default withRouter(ContactData);