import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import ContactData from './ContactData/ContactData';

class Checkout extends Component {
   state = {
      ingredients: {
         salad: 0,
         meat: 0,
         cheese: 0,
         bacon: 0
      },
      price: 0,
      totalIngredients: 0
   }
   componentDidMount() {
      const query = new URLSearchParams(this.props.location.search);
      let ingredients = {};
      let price = 0;
      let totalIngredients = 0
      let c = 0;
      for (let param of query.entries()) {
         // ['salad', '1']
         if(param[0]!=='price'){
            ingredients[param[0]] = +param[1];
            totalIngredients = totalIngredients + parseInt(param[1])
         }else{
            price = param[1];
         }
         c = c + 1;
      }
      if(c !== 0){
         localStorage.setItem('ingredients', JSON.stringify(ingredients));
         localStorage.setItem('price', price.toString());
      }else{
         ingredients = JSON.parse(localStorage.getItem('ingredients'));
         price = parseInt(localStorage.getItem('price'));
         console.log(ingredients);
         console.log('dari local storage');
      }
      this.setState({ ingredients: ingredients, price:price, totalIngredients:totalIngredients });
   }

   componentWillUnmount(){
      localStorage.clear();
   }

   checkoutCancelHanlder = () => {
      this.props.history.goBack();
   }

   checkoutContinuedHandler = () => {
      
      if(this.state.totalIngredients>0){
         this.props.history.push('/checkout/contact');
      }
      // this.props.history.replace('/checkout/contact');
      // console.log(this.props.match.path);
   }
   render() {
      return (
         <div>
            <CheckoutSummary
               ingredients={this.state.ingredients} 
               onCheckoutCanceled={this.checkoutCancelHanlder} 
               onCheckoutContinued={this.checkoutContinuedHandler} />
            {/* <Route path={this.props.match.path + "/contact"} component={ContactData} /> */}
            <Route path={this.props.match.path + "/contact"} render={()=><ContactData ingredients={this.state.ingredients} price={this.state.price}/>}/>
         </div>
      );
   }
}

export default Checkout;