import React, { Component } from 'react';
import incAxios from '../../axios-orders';

import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Burger from '../../components/Burger/Burger';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Modal from '../../components/UI/Modal/Modal';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';


const INGREDIENTS_PRICES = {
   salad: 5000,
   cheese: 4000,
   meat: 13000,
   bacon: 7000
};

class BurgerBuilder extends Component {
   state = {
      ingredients: null,
      totalPrice: 20000,
      purchasable: false,
      purchasing: false,
      loading: false,
      error: false
   }

   componentDidMount(){
      incAxios.get('/ingredients.json')
         .then(res=>{
            this.setState({ingredients: res.data});
         }).catch(err=>{
            this.setState({ error: true });
         });
   }


   updatedPurchaseState(ingredients) {
      const sum = Object.keys(ingredients)
         .map(igKey => {
            return ingredients[igKey];
         }).reduce((sum, el) => {
            return sum + el;
         }, 0);
      this.setState({ purchasable: sum > 0 })
   }

   addIngredientHandler = (type) => {
      const oldCount = this.state.ingredients[type];
      const updatedCount = oldCount + 1;
      const updatedIngredients = {
         ...this.state.ingredients
      };
      updatedIngredients[type] = updatedCount;
      const priceAddition = INGREDIENTS_PRICES[type];
      const oldPrice = this.state.totalPrice;
      const newPrice = oldPrice + priceAddition;
      this.setState({ totalPrice: newPrice, ingredients: updatedIngredients });
      this.updatedPurchaseState(updatedIngredients);
   }

   removeIngredientHandler = (type) => {
      const oldCount = this.state.ingredients[type];
      if (oldCount <= 0) {
         return;
      }
      const updatedCount = oldCount - 1;
      const updatedIngredients = {
         ...this.state.ingredients
      };
      updatedIngredients[type] = updatedCount;
      const priceAddition = INGREDIENTS_PRICES[type];
      const oldPrice = this.state.totalPrice;
      const newPrice = oldPrice - priceAddition;
      this.setState({ totalPrice: newPrice, ingredients: updatedIngredients });
      this.updatedPurchaseState(updatedIngredients);
   }

   purchasHandler = () => {
      this.setState({purchasing:true});
   }

   purchaseCancelHandler = () => {
      this.setState({purchasing:false});
   }

   purchaseContinueHandler = () => {
      // alert('You continue');
      // this.setState({loading: true});
      // const order = {
      //    ingredients: this.state.ingredients,
      //    totalPrice: this.state.totalPrice,
      //    customer: {
      //       name: 'Fandi',
      //       address: {
      //          jalan: 'Jl Kebonsari',
      //          kota: 'Surabaya'
      //       },
      //       email: 'test@testmail.com'
      //    },
      //    deliverMethod: 'fastest'
      // }
      // incAxios.post('/orders.json', order).then(res =>{
      //    // console.log(res);
      //    this.setState({
      //       loading: false, 
      //       purchasing: false, 
      //       ingredients: {
      //          salad: 0,
      //          bacon: 0,
      //          cheese: 0,
      //          meat: 0
      //       },
      //       totalPrice: 20000,});
      // }).catch(err=>{
      //    // console.log(err);
      //    this.setState({ loading: false, purchasing: false });
      // });
      const queryParams = [];
      for (let i in this.state.ingredients){
         queryParams.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i]));
      }
      queryParams.push('price='+this.state.totalPrice);
      const queryString = queryParams.join('&');
      this.props.history.push({
         pathname: '/checkout',
         search: '?'+queryString
      });
   }

   render() {
      const disabledInfo = {
         ...this.state.ingredients
      };
      for (let key in disabledInfo) {
         disabledInfo[key] = disabledInfo[key] === 0;
      }
      let orderSummary = null;

      let burger = this.state.error? <p style={{width:'100%', textAlign:'center'}}>Ingredients can't be loaded!</p>:<Spinner/>

      if(this.state.ingredients){
         burger = (
            <>
               <Burger ingredients={this.state.ingredients} />
               <BuildControls
                  addIngredient={this.addIngredientHandler}
                  removeIngredient={this.removeIngredientHandler}
                  disabled={disabledInfo}
                  purchasable={!this.state.purchasable}
                  ordered={this.purchasHandler}
                  totalPrice={this.state.totalPrice}
               />
            </>
         );
         orderSummary = <OrderSummary
                           ingredients={this.state.ingredients}
                           purchaseCancel={this.purchaseCancelHandler}
                           purchaseContinue={this.purchaseContinueHandler}
                           totalPrice={this.state.totalPrice}
                        />
      }

      if (this.state.loading) {
         orderSummary = <Spinner />
      }

      return (
         <>
            <Modal show={this.state.purchasing} modalClose={this.purchaseCancelHandler}>
               {orderSummary}
            </Modal>
            {burger}
         </>
      );
   }
}

export default withErrorHandler(BurgerBuilder, incAxios);