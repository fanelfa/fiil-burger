import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import Checkout from './containers/Checkout/Checkout';
// import ContactData from './containers/Checkout/ContactData/ContactData';
import Layout from './containers/Layout/Layout';
import Orders from './containers/Orders/Orders';

function App() {

   return (
      <BrowserRouter>
         <div>
            <Layout>
               <Switch>
                  {/* <Route path="/checkout/contact" exact component={ContactData} /> */}
                  <Route path="/checkout" component={Checkout} />
                  <Route path="/orders" component={Orders} />
                  <Route path="/" exact component={BurgerBuilder} />
               </Switch>
            </Layout>
         </div>
      </BrowserRouter>
   );
}

export default App;
