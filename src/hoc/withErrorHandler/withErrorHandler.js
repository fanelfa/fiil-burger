import React, { Component } from 'react';
import Modal from '../../components/UI/Modal/Modal';

const withErrorHandler = (WrappedComponent, axios) => {
   return class extends Component{
      state ={
         error: null
      }
      
      componentDidMount(){
         this.reqInterceptor = axios.interceptors.request.use(req=>{
            this.setState({ error: null });
            return req;
         });
         this.resInterceptor = axios.interceptors.response.use(res => res, err => {
            this.setState({error: err});
         });
      }

      componentWillUnmount(){
         // console.log('will unmount');
         axios.interceptors.request.eject(this.reqInterceptor);
         axios.interceptors.response.eject(this.resInterceptor);
      }

      errorConfirmedHandler= () => {
         this.setState({error: null});
      }

      render(){
         return (
            <>
               <Modal 
                  show={this.state.error}
                  modalClose={this.errorConfirmedHandler}>
                  <h4 style={{ color: 'red', textAlign: 'center' }}>
                     {this.state.error ? this.state.error.message : null}
                  </h4>
               </Modal>
               <WrappedComponent {...this.props} />
            </>
         );
      }
   }
};


export default withErrorHandler;